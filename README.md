WebdriverIO-Cucumber
====================

## Requirements

- Node version 6 or higher
- Yarn (>= 1.0.0)

## Quick start

Proceed through the following steps:

1. Clone the git repo — `git clone ssh://git@git.enroutesystems.com:2222/eisaaclrubio/WebdriverIO.git`

2. Switch to the project of your choice 
    * `git checkout webdriverio-cucumber-framework`
    * `git checkout webdriverio-chimp`

2. Install the dependencies (`yarn install`)

Now you are ready to write your own features.

# How to run the test

To run your web app tests just call the prewritten scripts as follows:

```sh
$ yarn test:web
```

If you want to test your android app call it as follows:
```sh
$ yarn test:android
```

# Pending test

If you have failing or unimplemented tests you can mark them as "pending" or "ignore" so they will get skipped.

```gherkin
// skip whole feature file
@pending
Feature: ...

// only skip a single scenario
@ignore
Scenario: ...
```
